# Valikhujaev Yakhyokhuja


### AI Research Engineer
[![](./icons/icons8-github.svg)](https://github.com/yakhyo) &nbsp;[![](./icons/icons8-linkedin.svg)](https://www.linkedin.com/in/y-valikhujaev/) &nbsp;[![](./icons/icons8-google-scholar.svg)](https://scholar.google.com/citations?user=I66QbJIAAAAJ&hl=en) &nbsp;[![](./icons/icons8-stack-overflow.svg)](https://stackoverflow.com/users/14815986/yakhyo) 
<!-- 
&nbsp;[![](./icons/icons8-youtube.svg)](https://youtube.com/codeuz) &nbsp;[![](./icons/icons8-medium.svg)](https://yakhyo.medium.com/) &nbsp;[![](./icons/icons8-twitter.svg)](https://twitter.com/y_valikhujaev) &nbsp;[![](./icons/icons8-facebook.svg)](https://www.facebook.com/yvalikhujaev/) &nbsp;[![](./icons/icons8-telegram.svg)](https://t.me/yakhyokhuja)
-->
## EXPERIENCE
### Divus Co, Ltd | 2020.11.18 - present
### AI Research Engineer
 - ANPR (Automatic Number Plate Recognition):
   - Object detection (YOLOv5s-l), Image Generation ([Generating synthetic number plate with YOLO format labels](https://github.com/yakhyo/Korean-License-Plate-Generator))
   - Building/Training/Inference. Deployment on Mobile/WEB (Rest APIs)
   - Integrating multi-stage models for Deployment
 - Car Damage detection:
   - Implementation of YOLOv5s for car damage detection.
   - Deployment on Android OS.
 - OCR
   - STD (Scene Text Detection): [detectron2](https://github.com/facebookresearch/detectron2), [EAST](https://github.com/yakhyo/EAST-pt), [CRAFT](https://github.com/yakhyo/ClovaAI-CRAFT), [TextFuseNet](https://github.com/ying09/TextFuseNet)
   - STR (Scene Text Recognition): STN, BiLSTM, Attention, CTC based methods
 - Shadow Removal
   - GANs, CycleGAN, MaskShadowGAN and etc.
 
### Gachon University | 2019.04.01 - 2020.01.12
### Teaching Assistant
Teaching middle school students (5 students). Smart City project
 - **C++/Python** fundamentals to code write code in **Arduino Uno** and **Raspberry Pi 2**.
 - Face detection using **OpenCV** and digit recognizer using **Machine Learning**.
 - Using **GPIO** ports of Raspberry PI to read and write data.
 - Using STT (Speech To Text) with Raspberry to control home appliances.
 - Won 3rd place among more than ten groups at Gachon University and participated national competition.

### Gachon University | 2019.04.01 - 2020.01.12
### Researcher
 - Computer Vision: Applying Machine Learning models into Computer Vision field.
 - Data Analysis: pandas, seaborn, matplotlib and etc.
 - Machine Learning based Recommendation System.
 - Android app to control Rice Peeling Machine via Bluetooth.
 - Building/Training/Inference and deployment of Deep learning models on Android OS and Edge devices.
 - Published a SCI [paper](https://www.mdpi.com/2073-4433/11/11/1241) and several domestic conference [papers](https://scholar.google.com/citations?user=I66QbJIAAAAJ&hl=en).

## EDUCATION
### MS in Computer Engineering, Gachon University | 2018.09.01 - 2021.02.24
 - GPA: 4.01 / 4.5
 - Best paper award from **FISK** (Fire Investigation Society of Korea).
 - Best presentation award from **ISIS2019 & ICBAKE2019**.
 - Thesis: Automatic fire and smoke detection method for surveillance systems based on dilated CNNs

### BS in Computer Engineering, TUIT | 2014.09.01 - 2018.06.12
 - GPA: 85 / 100
 - Studied C++/Java/PHP/JavaScript, Algorithms, Dynamic Programming, Web Programming and etc.
 - Projects:
   - Desktop UI for English language learners with a dictionary more than 25k words using C++ Builder 6 and Embarcadero C++.
   - Website: Car Sale, Concrete companies web page design.
   - Air Conditioning system in green houses using Embedded device (Arduino Uno).
 

## SKILLS
### Technical skills:
 - Programming Languages: Python/C++/Java
 - Technologies: PyTorch, Tensorflow, Keras
 - Data Analysis: pandas, matplotlib, seaborn
 - Coding: Problem-solving, Competitive programming
 - Linear Algebra Statistics, Calculus, Discrete Mathematics
### Languages:
 - English - Professional working proficiency
 - Korean - Limited working proficiency
 - Russian - Elementary working proficiency
 - Uzbek - Native
 

## PERSONAL QUALITIES:
 - Determined and decisive: Uses initiative to develop effective solutions to problems.
 - Reliable and Dependable: High personal standards and attention to deal.
 - Emotionally mature: Calming and positive temperament, tolerant and understanding.
 - Strong planning: Organizing and monitoring abilities.

## INTERESTS AND ACTIVITIES
 - Machine Learning / Deep Learning / Computer Vision / General AI / Cyber Security
 - Research & Development
 - Hiking & Travelling



Download in [PDF](./assets/cv_github.pdf) | [PNG](./assets/cv_github.png) format.
